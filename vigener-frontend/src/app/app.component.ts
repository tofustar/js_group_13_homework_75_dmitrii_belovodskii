import { Component, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Message } from './model/message.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  @ViewChild('encode') encode!: ElementRef;
  @ViewChild('decode') decode!: ElementRef;
  @ViewChild('password') password!: ElementRef;

  constructor(private http: HttpClient) {}

  encodeMsg() {
    const encode = {
      "password": this.password.nativeElement.value,
      "message": this.encode.nativeElement.value,
    }

    this.http.post<Message>("http://localhost:8000/encode", encode).subscribe( data => {
      this.decode.nativeElement.value = data.code;
    });
  }

  decodeMsg() {
    const decode = {
      "password": this.password.nativeElement.value,
      "message": this.decode.nativeElement.value,
    }

    this.http.post<Message>("http://localhost:8000/decode", decode).subscribe(data => {
      this.encode.nativeElement.value = data.code;
    });
  }

}
