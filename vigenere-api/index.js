const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');
const app = express();
const port = 8000;

app.use(express.json());
app.use(cors());

app.get('/encode/:something', (req, res) => {
  const getEncode = Vigenere.Cipher(password).crypt(`${req.params.something}`);
  res.send(`${req.params.something} to encode: ${getEncode}`);
});

app.get('/decode/:something', (req, res) => {
  const getDecode = Vigenere.Decipher(password).crypt(`${req.params.something}`);
  res.send(`${req.params.something} to decode: ${getDecode}`);
});

app.post('/encode', (req, res) => {

  const postEncode = {
    password: req.body.password,
    message: req.body.message
  };

  const msgEncode = Vigenere.Cipher(postEncode.password).crypt(postEncode.message);

  res.send({"code" : msgEncode});
});

app.post('/decode', (req, res) => {

  const postDecode = {
    password: req.body.password,
    message: req.body.message
  };

  const msgDecode = Vigenere.Decipher(postDecode.password).crypt(postDecode.message);

  res.send({"code" : msgDecode});
});

app.listen(port, () => {
  console.log('We are live on ' + port);
});